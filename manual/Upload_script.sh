!connect jdbc:hive2://localhost:10000

create table meta_tmp (
asin string,
title string,
price FLOAT,
imUrl string,
brand string,
categories array<array<string>>
)
ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe';

LOAD DATA INPATH '/tmp/data/meta_Amazon_Instant_Video.json' INTO TABLE meta_tmp;


create table review_tmp (
reviewerID string,
asin string,
reviewerName string,
reviewText string,
overall FLOAT,
summary string
)
ROW FORMAT SERDE 'org.apache.hive.hcatalog.data.JsonSerDe';

LOAD DATA INPATH '/tmp/data/reviews_Amazon_Instant_Video.json' INTO TABLE review_tmp;